import React, {Component} from 'react'
import {Dimensions, I18nManager} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import {Router, Scene, Actions, Drawer, Lightbox, Stack} from 'react-native-router-flux'
import splash from "./pages/splash";
import Introduce from "./pages/introduce";
import Login from "./pages/login";
import Home from "./pages/home";
import AccidentSteps from "./pages/accidentSteps";
import InfoComplete from "./pages/infoComplete";
import {Root} from "native-base";

EStyleSheet.build({
    $font :'IRANYekanMobile',
    $mainColor :'rgb(240, 242, 243)',
    $appColor: 'rgb(48, 191, 199)',
    $darkColor: 'rgb(70, 70, 70)',
    $successColor: 'rgb(13, 214, 168)',
    $processColor: 'rgb(197, 167, 98)',
    $fontSize: 14,
})

export default class App extends Component{
    componentWillMount(){
        I18nManager.allowRTL(false)
    }
    componentDidMount(){
        I18nManager.allowRTL(false)
    }
    componentWillUnmount(){
        I18nManager.allowRTL(false)
    }
    render(){
        return(
            <Root>
                <Router>
                    <Stack key="root">
                        <Scene key="splash" component={splash} hideNavBar  />
                        <Scene key="introduce" component={Introduce} hideNavBar />
                        <Scene key="login" component={Login} hideNavBar  />
                        <Scene key="home" component={Home} hideNavBar />
                        <Scene key="accidentSteps" component={AccidentSteps} hideNavBar  />
                        <Scene key="infoComplete" component={InfoComplete} hideNavBar initial />
                    </Stack>
                </Router>
            </Root>
        )
    }
}