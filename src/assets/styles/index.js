import EStyleSheet from 'react-native-extended-stylesheet';

export const splashSC = EStyleSheet.create({
    bold:{
        '@media ios' : {
            fontWeight : "bold"
        },
        '@media android' : {
            fontFamily : 'IRANYekanMobileBold',
        },
    },
    container: {backgroundColor: 'white', flex: 1},
    copyrightParent:{justifyContent:'center',alignItems:'center',flex:.25},
    version:{fontSize:12,color:'#8a8a8a'},
    design:{fontSize:'$fontSize', fontFamily: '$font', color: 'rgb(110, 110, 110)'}
});
export const introduce = EStyleSheet.create({
    container: {backgroundColor: 'white', flex: 1},
    image:{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'flex-end'},
    slideContainer: {width: '100%', paddingBottom: 40},
    sliderText: {textAlign: 'center', fontFamily: '$font', padding: 30, paddingBottom: 50,  lineHeight: 30}
});
export const login = EStyleSheet.create({
    container: {backgroundColor: 'white', flex: 1},
    image:{flex: 1, alignItems: 'center'},
    logo:{width: '40%', resizeMode: 'contain', alignSelf: 'center', marginTop: 50},
    telLabel: {color: 'gray', fontFamily: '$font', fontSize: '$fontSize', paddingRight: 5},
    telContainer: {flexDirection: 'row', alignItems: "flex-end", justifyContent: 'space-between', borderBottomColor: 'rgb(172, 172, 172)', borderBottomWidth: 3, width: '70%'},
    icon: {borderRadius: 100, borderWidth: 1, borderColor: 'gray', padding: 2},
    inputs: {width: '90%', borderRadius: 5, backgroundColor: 'white', height: 36, flexDirection: 'row', alignItems: 'center', paddingRight: 10, paddingLeft: 10, marginBottom: 15 },
    nameInput: {fontSize: '$fontSize', textAlign: 'right', paddingRight: 10, fontFamily: '$font', height: 36, paddingLeft: 15},
    telInput: {fontSize: '$fontSize', textAlign: 'left', paddingRight: 10, fontFamily: '$font', height: 36, paddingLeft: 15},
    button: {backgroundColor: '$appColor', borderRadius: 20, width: '90%', height: 35, alignItems: 'center', justifyContent: 'center', marginTop: 20},
    title: {fontSize: '$fontSize', fontFamily: '$font', color: 'white', textAlign: 'center'},
});
export const headerSt = EStyleSheet.create({
    container: {backgroundColor: 'white', position: 'relative', zIndex: 1, height: 90},
    icon: {color: '$darkColor'},
    image: {position: 'absolute', top: -35, left: '35%', zIndex: 3}
});
export const home = EStyleSheet.create({
    container: {flex: 1, backgroundColor: '$mainColor'},
    icon: {color: '$darkColor'},
    image: {position: 'absolute', top: -35, left: '35%', zIndex: 3},
    accident: {alignSelf: 'center', width: 160, resizeMode: 'contain'},
    footerImage: {width: '100%', resizeMode: 'contain'},
    label: {fontSize: 13, fontFamily: '$font', color: '$darkColor', paddingLeft: 5},
    userInfo: {backgroundColor: 'white', elevation: 5, borderBottomLeftRadius: 50, width: '60%', alignSelf: 'flex-end', padding: 20, marginTop: 30},
    scroll: {paddingTop: 0, paddingRight: 20, paddingLeft: 20, position: 'absolute', top: '70%', bottom: 0, width: '100%'},
    content: {paddingTop: 10, width: '100%'}
});
export const footerSt = EStyleSheet.create({
    footerContainer: {position: 'absolute', bottom: 0, left: 0, right: 0, zIndex: 3},
    footer: {backgroundColor: 'transparent', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', height: 60},
    image: {position: 'absolute', top: -35, left: '35%', zIndex: 3},
    footerImage: {position: 'absolute', bottom: -14, left: 0, right: 0, zIndex: -1, width: '100%', resizeMode: 'contain'},
});
export const news = EStyleSheet.create({
    container: {backgroundColor: 'white', padding: 10, marginBottom: 15, borderRadius: 10},
    image: {width: 90, height: 90, resizeMode: 'stretch'},
    content: {width: '70%', justifyContent: 'flex-start', alignItems: 'flex-end'},
    title: {fontSize: '$fontSize', fontFamily: '$font', color: 'black', paddingRight: 10, paddingBottom: 10},
    contentText: {fontSize: 12, fontFamily: '$font', color: '$darkColor', paddingRight: 10},
    date: {position: 'absolute', top: 10, left: 15, zIndex: 50, fontSize: 10, fontFamily: '$font'},

});
export const accidentSteps = EStyleSheet.create({
    container: {flex: 1, backgroundColor: '$mainColor', position: 'relative', zIndex: 1},
    scrollContent: {width: '100%', alignItems: 'center', justifyContent: 'center', paddingTop: 20, paddingBottom: 100},
    imageContainer: {position: 'relative', zIndex: 0},
    clockText: {position: 'absolute', top: 85, left: 47, zIndex: 50, color: 'rgb(48, 191, 199)', fontFamily: '$font', fontSize: 18},
    phone: {position: 'absolute', top: 60, left: 50, zIndex: 50},
    contentText: {fontSize: '$fontSize', fontFamily: '$font', color: '$darkColor', paddingRight: 10},
    label: {fontSize: 13, fontFamily: '$font', color: '$darkColor'},
    step: {backgroundColor: 'white', padding: 20, marginBottom: 15, borderRadius: 10, width: '75%', alignItems: 'center', justifyContent: 'center',},
    stepImage: {width: 50, resizeMode: 'contain', position: 'absolute', top: -10, right: 0, zIndex: 50}

});
export const infoComplete = EStyleSheet.create({


    container: {flex: 1, backgroundColor: '$mainColor', position: 'relative', zIndex: 1},
    scrollContent: {width: '100%', alignItems: 'center', justifyContent: 'center', paddingTop: 20, paddingBottom: 100},



    imageContainer: {position: 'relative', zIndex: 0},
    clockText: {position: 'absolute', top: 85, left: 47, zIndex: 50, color: 'rgb(48, 191, 199)', fontFamily: '$font', fontSize: 18},
    phone: {position: 'absolute', top: 60, left: 50, zIndex: 50},
    contentText: {fontSize: '$fontSize', fontFamily: '$font', color: '$darkColor', paddingRight: 10},
    label: {fontSize: 13, fontFamily: '$font', color: '$darkColor'},
    step: {backgroundColor: 'white', padding: 20, marginBottom: 15, borderRadius: 10, width: '75%', alignItems: 'center', justifyContent: 'center',},
    stepImage: {width: 50, resizeMode: 'contain', position: 'absolute', top: -10, right: 0, zIndex: 50}

});
export const processIcon = EStyleSheet.create({
    container: {width: '100%', alignItems: 'flex-end', justifyContent: 'center', paddingRight: 30},
    iconContainer: {width: 60, height: 60, borderRadius: 100, alignItems: 'center', justifyContent: 'center', backgroundColor: '$successColor'},
    contentText: {fontSize: '$fontSize', fontFamily: '$font', color: '$darkColor', paddingRight: 20},
    row: {flexDirection: 'row', alignItems: 'center', justifyContent: 'center'},
    bordered: {height: 40, borderColor: '$successColor', borderRightWidth: 10, marginRight: 25, }

});
export default styles = {
    splashSC, introduce, login, headerSt, home, footerSt, news, accidentSteps, processIcon, infoComplete
};