/**
 * Created by n.moharami on 8/7/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity, View} from "react-native";
import {Text, Item } from 'native-base';
import {processIcon} from "../assets/styles";
import car from '../assets/images/icons/car-collision.png'
import support from '../assets/images/icons/support.png'
import resume from '../assets/images/icons/resume.png'

export default class ProcessIcon extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <View style={processIcon.container}>
                <View style={processIcon.row}>
                    <Text style={[processIcon.contentText, {color: this.props.process ? 'rgb(197, 167, 98)' : undefined}]}>{this.props.label}</Text>
                    <View style={[processIcon.iconContainer, {borderWidth: this.props.process ? 2 : undefined, borderColor: this.props.process ? 'rgb(197, 167, 98)' : undefined, backgroundColor: this.props.process ? 'white' : undefined}]}>
                        <Image source={this.props.img1 ? car : this.props.img2 ? support : resume} style={[processIcon.image, {tintColor: this.props.process ? 'rgb(197, 167, 98)' : undefined}]} />
                    </View>
                </View>
                {
                    this.props.content  ?
                        <View style={processIcon.borderedContent} />
                        :
                        <View style={[processIcon.bordered, {borderColor: this.props.process ? 'rgb(197, 167, 98)' : 'rgb(13, 214, 168)'}]} >
                        </View>
                }

            </View>
        )
    }
}