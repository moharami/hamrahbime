// axios
import Axios from 'axios'
import {AsyncStorage} from "react-native";

Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export const url = 'http://fabrica.ir/';
Axios.defaults.baseURL = url;

export function object2formData(obj) {
    let formData = new FormData()
    for (let i in obj) {
        formData.append(i, obj[i])
    }
    return formData
}


export async function pexios(url, method, data = {}, file = false) {
    const options = {
        method: method,
        data: data,
        url,
        headers: {
            contentType: '',
            Authorization: ''
        }
    };
    let token = await AsyncStorage.getItem('token')
    options.headers['Authorization'] = 'Bearer ' + token
    if (!file) {
        options.headers['content-type'] = 'application/json'
    } else {
        options.headers['content-type'] = 'multipart/form-data'
    }
    return Axios(options)
}

export function priceFormat(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

}