/**
 * Created by n.moharami on 8/6/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import { Container, Header, Content, Footer, FooterTab, Button, Icon, View } from 'native-base';
import {footerSt} from "../assets/styles";
import appLogo from "../assets/images/appLogoHeader.png";
import AIcon from 'react-native-vector-icons/dist/AntDesign'
import FIcon from 'react-native-vector-icons/dist/FontAwesome5'
import footer from '../assets/images/footer3.png'

export default class AppFooter extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
        <View style={footerSt.footerContainer}>
            <Image source={footer} style={footerSt.footerImage} />
            <View style={footerSt.footer}>
                <FIcon name="user" size={21} color="white" />
                <FIcon name="car-crash" size={25} color="black" />
                <AIcon name="infocirlceo" size={25} color="white" />
            </View>
        </View>
        )
    }
}
