/**
 * Created by n.moharami on 8/6/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import {Container, Icon, Left, Button,Title,Body, Right, Header} from "native-base";
import {headerSt} from "../assets/styles";
import appLogo from "../assets/images/appLogoHeader.png";
import AIcon from 'react-native-vector-icons/dist/AntDesign'

export default class AppHeader extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <Header style={headerSt.container} androidStatusBarColor="transparent">
                <Left>
                    <Button transparent>
                        <AIcon name='leftcircleo' size={20} style={headerSt.icon} />
                    </Button>
                </Left>
                <Body>
               <Image source={appLogo} style={headerSt.image} />
                </Body>
                <Right>
                    <Button transparent>
                        <Icon name='menu' style={headerSt.icon} />
                    </Button>
                </Right>
            </Header>
        )
    }
}
