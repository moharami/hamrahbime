import React, {Component} from 'react';
import { Container , View , Text , Body } from 'native-base';
import {filter, splashSC} from "../assets/styles";
import {AsyncStorage, Image, StatusBar} from "react-native";
import { Actions } from 'react-native-router-flux';
import {pexios} from "../component/helper";

export default class Splash extends Component {
    constructor(props){
        super(props)
        this.state={
            version:'1.0',
            needUpdate:false,
            url:null
        }
    }
    componentDidMount() {
        setTimeout( () => {
            this.redirect();
        },3000);
    }
    redirect () {
        Actions.introduce();
    }
    render() {
        return (
            <Container style={splashSC.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content"/>
                <View style={{flex:.25}}></View>
                <Body>
                    <Image source={require('../assets/images/appLogo.png')} />
                </Body>
                <View style={splashSC.copyrightParent}>
                    <Text style={splashSC.version}>version {this.state.version}</Text>
                    <Text style={splashSC.design}>designed by azarinweb</Text>
                </View>
            </Container>
        )
    }
}
