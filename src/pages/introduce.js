/**
 * Created by n.moharami on 8/5/2019.
 */
import React, {Component} from 'react';
import { Container , View , Text , Body } from 'native-base';
import {introduce} from "../assets/styles";
import {AsyncStorage, Image, StatusBar, BackHandler, ImageBackground, Dimensions } from "react-native";
import { Actions } from 'react-native-router-flux';
import {pexios} from "../component/helper";
import bg from '../assets/images/bg.png'
import car1 from '../assets/images/sliderImage/car1.png'
import car2 from '../assets/images/sliderImage/car2.png'
import car3 from '../assets/images/sliderImage/car3.png'
import car4 from '../assets/images/sliderImage/car4.png'
import car5 from '../assets/images/sliderImage/car5.png'
import Carousel from 'react-native-banner-carousel';
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;

export default class Introduce extends Component {
    constructor(props){
        super(props)
        this.state={
            version:'1.0'
        }
        this.onBackPress = this.onBackPress.bind(this);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    renderPage(image, index) {
        return (
            <View key={index}>
                <Image style={{ alignSelf: 'center' }} source={image} />
                <Text style={introduce.sliderText}>سریع ترین روش ممکن و دلخواه مشتری سریع ترین روش ممکن و دلخواه مشتری </Text>
            </View>
        );
    }
    render() {

        const images = [
            car1, car2, car3, car4, car5
        ];
        return (
            <Container style={introduce.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content"/>
                <ImageBackground source={bg} style={introduce.image}>
                    <View style={introduce.slideContainer}>
                        <Carousel
                            autoplay
                            autoplayTimeout={5000}
                            loop
                            index={0}
                            pageSize={BannerWidth}
                        >
                            {images.map((image, index) => this.renderPage(image, index))}
                        </Carousel>
                    </View>
                </ImageBackground>
            </Container>
        )
    }
}