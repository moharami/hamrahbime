/**
 * Created by n.moharami on 8/5/2019.
 */
import React, {Component} from 'react';
import { Container , View , Text , Item, Input, Button } from 'native-base';
import {login} from "../assets/styles";
import {AsyncStorage, Image, StatusBar, BackHandler, ImageBackground, Dimensions, TextInput } from "react-native";
import { Actions } from 'react-native-router-flux';
import {pexios} from "../component/helper";
import bg from '../assets/images/loginBg2.png'
import logo from '../assets/images/appLogo.png'
import Icon from 'react-native-vector-icons/dist/Feather';
import AIcon from 'react-native-vector-icons/dist/AntDesign';

export default class Login extends Component {
    constructor(props){
        super(props)
        this.state={
            mobile: '',
            name: ''
        }
        this.onBackPress = this.onBackPress.bind(this);

    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <Container style={login.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content"/>
                <ImageBackground source={bg} style={login.image}>
                    <Image source={logo} style={login.logo} />
                    <View regular style={login.inputs}>
                        <Input style={login.nameInput} autoFocus maxLength={11} keyboardType="numeric" onChangeText={(value)=> this.setState({name: value})} />
                        <Text style={[login.telLabel, {fontSize: 10}]}>نام و نام خانوادگی</Text>
                        <AIcon name="user" size={18} color={'gray'} />
                    </View>
                    <View regular style={login.inputs}>
                        <Text style={login.telLabel}>+98</Text>
                        <Input style={login.telInput} maxLength={11} keyboardType="numeric" onChangeText={(value)=> this.setState({name: value})} />
                        <Text style={[login.telLabel, {fontSize: 11}]}>شماره همراه </Text>
                        <Icon name="phone-call" size={14} color={'gray'} style={login.icon} />
                    </View>
                    <Button style={login.button}><Text style={login.title}> مرحله بعد </Text></Button>
                </ImageBackground>
            </Container>
        )
    }
}
