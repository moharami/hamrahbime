/**
 * Created by n.moharami on 8/7/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity, StatusBar, ScrollView } from "react-native";
import {Container, View, Item, Text} from "native-base";
import {infoComplete} from "../assets/styles";
import AppHeader from '../component/appheader'
import clock from '../assets/images/clock2.png'
import step1 from '../assets/images/step1.png'
import step2 from '../assets/images/step2.png'
import step3 from '../assets/images/step3.png'
import heart from '../assets/images/heart.png'
import AppFooter from '../component/appFooter'
import ProcessIcon from '../component/processIcon'
import Icon from 'react-native-vector-icons/dist/MaterialIcons'

export default class InfoComplete extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <Container style={infoComplete.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content" />
                <AppHeader />
                <ScrollView>
                    <View style={infoComplete.scrollContent}>
                        <ProcessIcon label="اعلام تصادف در 13:48 (1398/2/5)" img1  />
                        <ProcessIcon label="تماس و تایید تصادف توسط مرکز" img2 />
                        <ProcessIcon label="منتظر تکمیل اطلاعات" process img3 />
                    </View>
                </ScrollView>
                <AppFooter />
            </Container>
        )
    }
}