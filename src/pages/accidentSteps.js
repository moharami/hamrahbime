/**
 * Created by n.moharami on 8/7/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity, StatusBar, ScrollView } from "react-native";
import {Container, View, Item, Text} from "native-base";
import {accidentSteps} from "../assets/styles";
import AppHeader from '../component/appheader'
import clock from '../assets/images/clock2.png'
import step1 from '../assets/images/step1.png'
import step2 from '../assets/images/step2.png'
import step3 from '../assets/images/step3.png'
import heart from '../assets/images/heart.png'
import AppFooter from '../component/appFooter'
import Icon from 'react-native-vector-icons/dist/MaterialIcons'

export default class AccidentSteps extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        return(
            <Container style={accidentSteps.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content" />
                <AppHeader />
                <ScrollView>
                    <View style={accidentSteps.scrollContent}>
                        <View style={accidentSteps.imageContainer}>
                            <Image source={clock}  />
                            <Icon name="phone-in-talk" size={20} color="rgb(48, 191, 199)" style={accidentSteps.phone} />
                            <Text style={accidentSteps.clockText}>4:21</Text>
                        </View>
                        <Text style={[accidentSteps.contentText, {paddingTop: 15}]}>اعلام تصادف در ساعت</Text>
                        <Text style={[accidentSteps.contentText, {paddingBottom: 25}]}>13:21</Text>
                        <Image source={heart}  />
                        <Text style={[accidentSteps.contentText, {paddingTop: 15, fontSize: 16, color: 'black'}]}>سریع ترین روش ممکن</Text>
                        <Text style={[accidentSteps.contentText, {paddingBottom: 25, fontSize: 12}]}>لورم ایپسورم متن دلخواه من</Text>
                        <Text style={[accidentSteps.contentText, {paddingTop: 15, fontSize: 15, color: 'black', alignSelf: 'flex-end', paddingBottom: 20, paddingRight: 20}]}>مراحل</Text>
                        <Item style={accidentSteps.step}>
                            <Image source={step1} style={accidentSteps.stepImage} />
                            <Text style={accidentSteps.label}>منتظر تماس ما باشید</Text>
                        </Item>
                        <Item style={accidentSteps.step}>
                            <Image source={step2} style={accidentSteps.stepImage} />
                            <Text style={accidentSteps.label}>تکمیل اطلاعات</Text>
                        </Item>
                        <Item style={accidentSteps.step}>
                            <Image source={step3} style={accidentSteps.stepImage} />
                            <Text style={accidentSteps.label}>ارسال تصاویر</Text>
                        </Item>
                    </View>
                </ScrollView>
                <AppFooter />
            </Container>
        )
    }
}