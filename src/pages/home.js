/**
 * Created by n.moharami on 8/6/2019.
 */
import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity, StatusBar, ScrollView } from "react-native";
import {Container, Icon, View, Item, Text} from "native-base";
import {home} from "../assets/styles";
import AppHeader from '../component/appheader'
import accident from '../assets/images/accident.png'
import AppFooter from '../component/appFooter'
import News from '../component/news'

export default class Home extends Component{
    constructor(props){
        super(props)
        this.state={
        }
    }
    render(){
        const { shop } = this.props;
        return(
            <Container style={home.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content" />
                <AppHeader />
                <Item style={home.userInfo}>
                    <Text style={home.label}>اطلاعات کاربری خود را تکمیل کنید!</Text>
                </Item>
                <Image source={accident} style={home.accident} />
                <Text style={[home.label, {color: 'black', fontSize: 14, paddingRight: 20}]}>اخبار و اطلاعیه ها</Text>
                <ScrollView style={home.scroll}>
                    <View style={home.content} >
                        <News />
                        <News />
                        <News />
                        <News />
                        <News />
                    </View>
                </ScrollView>
                <AppFooter />
            </Container>
        )
    }
}